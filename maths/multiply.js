class Multiply {
	constructor() {
	}
	
	generate(divId, taskDivId) {
		this.timestart = new Date().getSeconds();
		let row = document.getElementById(divId).value;
		this.x = parseInt((!isNaN(parseInt(row))) ? row : Math.random() * 10);
		this.y = parseInt(Math.random() * 10);
		this.x = this.x < 1 ? 9 : this.x;
		this.y = this.y < 1 ? 9 : this.y;
		this.task(taskDivId);
	}
	
	task(divId) {
		document.getElementById(divId).innerHTML = "";
		document.getElementById(divId).insertAdjacentHTML("afterbegin", this.x + ' * ' + this.y);
		document.getElementById(divId).style.color = "blue";
	}
	
	check(value, divId, taskDivId, rowDivId) {
		let timeend = new Date().getSeconds();
		let correct = false;
		if (parseInt(value) == (this.x * this.y)) {
			correct = true;
		}
		let color = "red";
		if (correct) {
			let duration = timeend - this.timestart;
			duration = duration < 0 ? 60 - duration : duration;
			if (duration <= 3) {
				color = "green";
			} else {
				color = "orange";
			}
			document.getElementById(divId).insertAdjacentHTML("afterbegin", "<span>(" + duration + "s)</span> ");
		}
		document.getElementById(divId).insertAdjacentHTML("afterbegin", correct ? "<span style=\"color: " + color + "\">richtig</span> " : "<span style=\"color: " + color + "\">falsch</span> ");
		this.generate(rowDivId, taskDivId);
	}
}